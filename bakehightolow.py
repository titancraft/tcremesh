import bpy
import math

from titancraftutils.utils import select_only, generate_random_string
from titancraftutils.stats import Stats

class BakeHighToLow(bpy.types.Operator):
    'Bake High to Low'
    bl_idname = 'object.bake_high_to_low'
    bl_label = 'Bake High to Low'
    bl_options = {'REGISTER', 'UNDO'}

    texture_dimension: bpy.props.EnumProperty(
        name='Size',
        items=[
            ('256', '256 x 256 px', ''),
            ('512', '512 x 512 px', ''),
            ('1024', '1024 x 1024 px', ''),
            ('2048', '2048 x 2048 px', ''),
            ('4096', '4096 x 4096 px', ''),
        ],
        default='1024',
    )
    margin: bpy.props.IntProperty(
        name='Margin',
        min=0,
        max=32,
        default=10,
        subtype='PIXEL',
    )
    bake_normals: bpy.props.BoolProperty(
        name='Normals',
        default=True,
    )
    bake_color: bpy.props.BoolProperty(
        name='Color Texture',
        default=False,
    )
    bake_combo: bpy.props.BoolProperty(
        name='Metallic/Roughness/AO Texture',
        default=False,
    )
    bake_ao: bpy.props.BoolProperty(
        name='Ambient Occlusion',
        default=False,
    )
    combine_multiple_bakes: bpy.props.BoolProperty(
        name='Combine bake results for multiple distances',
        default=True,
    )
    num_steps: bpy.props.IntProperty(
        name='Steps',
        min=2,
        max=10,
        step=1,
        default=2,
    )
    ray_distance: bpy.props.FloatProperty(
        name='Ray Distance',
        min=0.01,
        step=0.01,
        default=0.25,
    )
    ray_distance_start: bpy.props.FloatProperty(
        name='Start',
        min=0.01,
        step=0.01,
        default=0.15,
    )
    ray_distance_end: bpy.props.FloatProperty(
        name='End',
        min=0.01,
        step=0.01,
        default=0.3,
    )
    attach_material: bpy.props.BoolProperty(
        name='Attach result material',
        default=True,
    )

    def execute(self, context):
        scene = context.scene
        self.dimension = int(self.texture_dimension)
        self.run_id = generate_random_string()
        self.orig_low_poly = scene.bakehightolow_low_poly
        self.orig_high_poly = scene.bakehightolow_high_poly
        if self.combine_multiple_bakes:
            self.ray_distances = get_ray_distances(self.ray_distance_start, self.ray_distance_end, self.num_steps)
        else:
            self.ray_distances = [round(self.ray_distance * 100) / 100]

        self.validate_args()
        self.save_state(context)

        self.bake_result = {}

        stats = Stats('bake')

        # Set up common bake settings
        with stats.time('preparebake'):
            self.prepare_bake(context)

        # Bake AO
        if self.bake_ao:
            with stats.time('bakeao'):
                self.do_ao_bake(context)

        # Bake normals
        if self.bake_normals:
            with stats.time('bakenormals'):
                self.do_normals_bake(context)

        # Bake color
        if self.bake_color:
            with stats.time('bakecolor'):
                source_colors = get_orig_color_image(self.orig_high_poly)
                self.do_color_bake(source_colors, 'color', context)

        # Bake combo
        if self.bake_combo:
            with stats.time('bakecombo'):
                source_colors = get_orig_combo_image(self.orig_high_poly)
                self.do_color_bake(source_colors, 'combo', context)

        # Attach / set up new material
        if self.attach_material:
            self.finalize_material()

        # Clean up
        select_only([self.high_poly, self.low_poly], context=context)
        bpy.ops.object.delete()
        bpy.data.materials.remove(self.high_poly_material)
        self.low_poly_material.node_tree.nodes.remove(self.texture_bake_node)
        self.restore_state(context)

        self.orig_low_poly.data['bake_result'] = self.bake_result
        self.orig_low_poly.data['bake_stats'] = stats

        return {'FINISHED'}

    def finalize_material(self):
        nodes = self.low_poly_material.node_tree.nodes
        links = self.low_poly_material.node_tree.links
        bsdf = nodes['Principled BSDF']
        color_name = self.bake_result.get('color')
        normals_name = self.bake_result.get('normals')
        combo_name = self.bake_result.get('combo')
        ao_name = self.bake_result.get('ao')

        # Attach color and/or AO
        if color_name is not None:
            color_node = nodes.new('ShaderNodeTexImage')
            color_node.image = bpy.data.images[color_name]
            if ao_name is not None:
                # Yes colors, yes AO
                ao_node = nodes.new('ShaderNodeTexImage')
                ao_node.image = bpy.data.images[ao_name]
                mix_node = nodes.new('ShaderNodeMix')
                mix_node.data_type = 'RGBA'
                mix_node.blend_type = 'MULTIPLY'
                mix_node.inputs['Factor'].default_value = 1
                links.new(mix_node.inputs[6], color_node.outputs['Color'])
                links.new(mix_node.inputs[7], ao_node.outputs['Color'])
                links.new(bsdf.inputs['Base Color'], mix_node.outputs['Result'])
            else:
                # Yes colors, no AO
                links.new(bsdf.inputs['Base Color'], color_node.outputs['Color'])
        elif ao_name is not None:
            # No colors, yes AO
            ao_node = nodes.new('ShaderNodeTexImage')
            ao_node.image = bpy.data.images[ao_name]
            links.new(bsdf.inputs['Base Color'], ao_node.outputs['Color'])

        # Attach normals
        if normals_name is not None:
            normal_map_node = nodes.new('ShaderNodeNormalMap')
            links.new(bsdf.inputs['Normal'], normal_map_node.outputs['Normal'])

            normals_node = nodes.new('ShaderNodeTexImage')
            normals_node.image = bpy.data.images[normals_name]
            links.new(normal_map_node.inputs['Color'], normals_node.outputs['Color'])

        if combo_name is not None:
            combo_node = nodes.new('ShaderNodeTexImage')
            combo_node.image = bpy.data.images[combo_name]
            separate_node = nodes.new('ShaderNodeSeparateColor')
            links.new(separate_node.inputs['Color'], combo_node.outputs['Color'])
            links.new(bsdf.inputs['Roughness'], separate_node.outputs['Green'])
            links.new(bsdf.inputs['Metallic'], separate_node.outputs['Blue'])

        # Add result material to low poly
        self.orig_low_poly.data.materials.clear()
        self.orig_low_poly.data.materials.append(self.low_poly_material)

    def prepare_bake(self, context):
        # Set up render and image settings
        scene = context.scene
        scene.render.engine = 'CYCLES'
        try:
            scene.cycles.device = 'GPU'
            scene.cycles.denoising_use_gpu = True
        except:
            pass
        scene.render.image_settings.file_format = 'PNG'
        scene.render.bake.use_selected_to_active = True
        bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[0].default_value = (1, 1, 1, 1)  # background brightness
        bpy.data.worlds['World'].light_settings.distance = 50

        # Duplicate high and low poly
        low_poly = duplicate_obj(self.orig_low_poly, context=context, name='_lowpoly')
        high_poly = duplicate_obj(self.orig_high_poly, context=context, name='_highpoly')
        low_poly.hide_render = False
        high_poly.hide_render = False
        low_poly.data.materials.clear()
        high_poly.data.materials.clear()
        self.high_poly = high_poly
        self.low_poly = low_poly

        # Set up material for high poly with no metallic and 100% roughness
        high_poly_material = bpy.data.materials.new('High Poly')
        high_poly_material.use_nodes = True
        high_poly_nodes = high_poly_material.node_tree.nodes
        high_poly_links = high_poly_material.node_tree.links
        high_poly.data.materials.append(high_poly_material)

        high_poly_bsdf = high_poly_nodes['Principled BSDF']
        high_poly_bsdf.inputs['Metallic'].default_value = 0
        high_poly_bsdf.inputs['Roughness'].default_value = 1
        high_poly_bsdf.inputs[12].default_value = 0  # IOR level
        self.high_poly_material = high_poly_material

        # Set up low poly material and generic texture node to bake to
        low_poly_material = bpy.data.materials.new('Bake Result')
        low_poly_material.use_nodes = True
        low_poly_nodes = low_poly_material.node_tree.nodes
        low_poly_bsdf = low_poly_nodes['Principled BSDF']
        low_poly_bsdf.inputs['Metallic'].default_value = 0
        low_poly_bsdf.inputs['Roughness'].default_value = 1
        low_poly_bsdf.inputs[12].default_value = 0  # IOR level
        low_poly.data.materials.append(low_poly_material)
        self.low_poly_material = low_poly_material

        self.texture_bake_node = low_poly_nodes.new('ShaderNodeTexImage')
        low_poly_nodes.active = self.texture_bake_node

    def do_ao_bake(self, context):
        # Adjust render settings
        context.scene.render.bake.cage_extrusion = 1
        context.scene.render.bake.max_ray_distance = 0
        context.scene.render.bake.margin = self.margin
        context.scene.cycles.use_fast_gi = True
        context.scene.cycles.use_denoising = True
        context.scene.render.bake.use_pass_direct = True
        context.scene.render.bake.use_pass_indirect = True
        context.scene.render.bake.use_pass_diffuse = True
        context.scene.render.bake.use_pass_glossy = False
        context.scene.render.bake.use_pass_transmission = False
        context.scene.render.bake.use_pass_emit = False
        context.scene.render.bake.use_clear = True
        context.scene.cycles.use_denoising = False  # otherwise edges get stripes around them, can't combine nicely
        context.scene.cycles.samples = 128

        # # Commented out 1/28/25 - don't use denoising on AO bakes, it messes
        # # up the edges and creates black streaks at seams.
        # # See https://projects.blender.org/blender/blender/issues/94573
        # if not self.combine_multiple_bakes:
        #     context.scene.cycles.use_denoising = True
        #     context.scene.cycles.samples = 32

        # Add floor
        bpy.ops.mesh.primitive_plane_add()
        floor = context.object
        floor.rotation_euler.x = math.pi
        floor.scale = (100, 100, 100)
        floor.location.z = min(min(vert[2] for vert in self.low_poly.bound_box) - 0.01, 0)

        select_only([self.low_poly, self.high_poly], context=context)

        images = []
        for ii, ray_distance in enumerate(self.ray_distances):
            is_last_image = ii == len(self.ray_distances) - 1
            name = f'{self.orig_low_poly.name} {self.run_id} ao {ray_distance}'
            context.scene.render.bake.cage_extrusion = ray_distance
            context.scene.render.bake.max_ray_distance = 0 if is_last_image else ray_distance
            context.scene.render.bake.margin = self.margin if is_last_image else 0  # only use margin on last image
            image = bpy.data.images.new(name, width=self.dimension, height=self.dimension, alpha=True)
            self.texture_bake_node.image = image
            images.append(image)
            bpy.ops.object.bake(type='COMBINED')

        name = f'{self.orig_low_poly.name} {self.run_id} ao'
        if len(self.ray_distances) == 1:
            result_image = images[0]
            result_image.name = name
        else:
            result_image = bpy.data.images.new(name, width=self.dimension, height=self.dimension, alpha=False)
            combine_color_images(images, result_image)
            # Clean up
            for image in images:
                bpy.data.images.remove(image)

        self.bake_result['ao'] = result_image.name

        # Clean up
        select_only([floor], context=context)
        bpy.ops.object.delete()

    def do_normals_bake(self, context):
        # Adjust render settings
        context.scene.render.bake.max_ray_distance = 0
        context.scene.cycles.use_denoising = False
        context.scene.cycles.samples = 1
        context.scene.render.bake.use_clear = True

        select_only([self.low_poly, self.high_poly], context=context)
        images = []
        for ii, ray_distance in enumerate(self.ray_distances):
            is_last_image = ii == len(self.ray_distances) - 1
            context.scene.render.bake.cage_extrusion = ray_distance
            context.scene.render.bake.margin = self.margin if is_last_image else 0  # only use margin on last image
            name = f'{self.orig_low_poly.name} {self.run_id} normals {ray_distance}'
            image = bpy.data.images.new(name, width=self.dimension, height=self.dimension, alpha=False)
            image.colorspace_settings.name = 'Non-Color'
            self.texture_bake_node.image = image
            images.append(image)
            bpy.ops.object.bake(type='NORMAL')

        name = f'{self.orig_low_poly.name} {self.run_id} normals'
        if len(self.ray_distances) == 1:
            result_image = images[0]
            result_image.name = name
        else:
            result_image = bpy.data.images.new(name, width=self.dimension, height=self.dimension, alpha=False)
            result_image.colorspace_settings.name = 'Non-Color'
            combine_normal_images(images, result_image)
            # Clean up
            for image in images:
                bpy.data.images.remove(image)

        self.bake_result['normals'] = result_image.name
        
    def do_color_bake(self, source_image, attribute_name, context):
        # Adjust render settings
        context.scene.cycles.use_fast_gi = False
        context.scene.render.bake.use_pass_direct = False
        context.scene.render.bake.use_pass_indirect = False
        context.scene.render.bake.use_clear = True
        context.scene.cycles.samples = 1
        if self.combine_multiple_bakes:
            context.scene.cycles.use_denoising = False  # otherwise edges get stripes around them, can't combine nicely
        else:
            context.scene.cycles.use_denoising = True

        # Attach source color texture to highres
        high_poly_nodes = self.high_poly_material.node_tree.nodes
        high_poly_links = self.high_poly_material.node_tree.links
        bsdf = high_poly_nodes['Principled BSDF']
        texture_node = high_poly_nodes.new('ShaderNodeTexImage')
        high_poly_links.new(bsdf.inputs['Base Color'], texture_node.outputs['Color'])
        texture_node.image = source_image

        select_only([self.low_poly, self.high_poly], context=context)
        images = []
        for ii, ray_distance in enumerate(self.ray_distances):
            is_last_image = ii == len(self.ray_distances) - 1
            name = f'{self.orig_low_poly.name} {self.run_id} {attribute_name} {ray_distance}'
            context.scene.render.bake.cage_extrusion = ray_distance
            context.scene.render.bake.max_ray_distance = 0 if is_last_image else ray_distance * 1.5
            context.scene.render.bake.margin = self.margin if is_last_image else 0  # only use margin on last image
            image = bpy.data.images.new(name, width=self.dimension, height=self.dimension, alpha=True)
            self.texture_bake_node.image = image
            images.append(image)
            bpy.ops.object.bake(type='DIFFUSE')

        name = f'{self.orig_low_poly.name} {self.run_id} {attribute_name}'
        if len(self.ray_distances) == 1:
            result_image = images[0]
            result_image.name = name
        else:
            result_image = bpy.data.images.new(name, width=self.dimension, height=self.dimension, alpha=False)
            combine_color_images(images, result_image)
            # Clean up
            for image in images:
                bpy.data.images.remove(image)

        self.bake_result[attribute_name] = result_image.name

    def save_state(self, context):
        self.orig_selection = context.selected_objects
        # Put active object at front
        self.orig_selection.insert(0, self.orig_selection.pop(self.orig_selection.index(context.object)))

        orig_hide_render = {}
        for obj in context.scene.objects:
            orig_hide_render[obj.name] = obj.hide_render
            obj.hide_render = True
        self.orig_hide_render = orig_hide_render

    def restore_state(self, context):
        # Restore original selection
        select_only(self.orig_selection, context=context)

        # Restore original hide render
        for obj_name, hide_render in self.orig_hide_render.items():
            context.scene.objects[obj_name].hide_render = hide_render

    def validate_args(self):
        if self.orig_high_poly is None or self.orig_low_poly is None or self.orig_high_poly is self.orig_low_poly:
            self.report({'ERROR_INVALID_CONTEXT'}, f'Must select two meshes.')
            return {'CANCELLED'}

        if len(self.orig_low_poly.data.uv_layers) == 0:
            self.report({'ERROR'}, f'Low poly object has no UVs!')
            return {'CANCELLED'}

    def invoke(self, context, event):
        # Try to set high and low poly arguments from current selection
        scene = context.scene
        selected = [obj for obj in context.selected_objects if obj.type == 'MESH']

        scene.bakehightolow_high_poly = None
        scene.bakehightolow_low_poly = None
        if len(selected) == 2:
            # If two objects are selected, look at vertex count to decide high/low poly
            selected.sort(key=lambda obj: len(obj.data.vertices))
            scene.bakehightolow_high_poly = selected[1]
            scene.bakehightolow_low_poly = selected[0]
        elif len(selected) == 1:
            # If only one objects selected, assume low poly and try to guess high poly
            obj = selected[0]
            scene.bakehightolow_low_poly = obj
            if 'lowres' in obj.name:
                name_orig = obj.name.replace('lowres', 'orig')
                name_high_poly = obj.name.replace('lowres', 'highres')
                if name_orig in scene.objects:
                    scene.bakehightolow_high_poly = scene.objects[name_orig]
                elif name_high_poly in scene.objects:
                    scene.bakehightolow_high_poly = scene.objects[name_high_poly]

        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        layout.separator()
        layout.separator()

        layout.prop(scene, 'bakehightolow_high_poly')
        layout.prop(scene, 'bakehightolow_low_poly')
        layout.separator()

        layout.prop(self, 'bake_normals')
        layout.prop(self, 'bake_color')
        layout.prop(self, 'bake_combo')
        layout.prop(self, 'bake_ao')
        layout.separator()

        dimensions = layout.row()
        dimensions.prop(self, 'texture_dimension')
        dimensions.prop(self, 'margin')

        layout.prop(self, 'combine_multiple_bakes')
        if self.combine_multiple_bakes:
            row = layout.row()
            row.label(text='Ray Distance')
            row.prop(self, 'ray_distance_start')
            row.prop(self, 'ray_distance_end')
            layout.prop(self, 'num_steps')

            steps = get_ray_distances(self.ray_distance_start, self.ray_distance_end, self.num_steps)
            layout.label(text='Distances: ' + ', '.join([str(step) for step in steps]))
        else:
            layout.prop(self, 'ray_distance')

        layout.separator()
        layout.prop(self, 'attach_material')

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    @classmethod
    def on_register(cls):
        bpy.types.Scene.bakehightolow_high_poly = bpy.props.PointerProperty(
            name='High Poly',
            type=bpy.types.Object,
            poll=filter_object,
        )
        bpy.types.Scene.bakehightolow_low_poly = bpy.props.PointerProperty(
            name='Low Poly',
            type=bpy.types.Object,
            poll=filter_object,
        )

    @classmethod
    def on_unregister(cls):
        try:
            del bpy.types.Scene.bakehightolow_high_poly
        except:
            pass
        try:
            del bpy.types.Scene.bakehightolow_low_poly
        except:
            pass

def get_ray_distances(start, end, num_steps):
    steps = []
    current = start
    step_size = (end - start) / (num_steps - 1)
    while start < end and current <= end:
        step = round(current * 100) / 100
        steps.append(step)
        current += step_size

    return steps

def combine_normal_images(source_images, result_image):
    target_r = 128/255
    target_g = 128/255
    target_b = 1

    def get_distance(rr, gg, bb):
        return math.sqrt((rr - target_r)**2 + (gg - target_g) ** 2 + (bb - target_b) **2)

    source_pixel_datas = [list(source_image.pixels) for source_image in source_images]  # list() is MUCH faster
    num_pixels = int(len(source_pixel_datas[0]) / 4)

    # Initialize pixel data for new image
    result_pixels = [1] * (num_pixels * 4)

    # Look at normals pixel by pixel. Find which image has pixel closest
    # to default normal color. Copy the pixel from that image to the result
    # image pixels.
    for r_index in range(0, num_pixels * 4, 4):
        g_index = r_index + 1
        b_index = r_index + 2
        best_distance = 9999
        for source_pixels in source_pixel_datas:
            normals_r = source_pixels[r_index]
            normals_g = source_pixels[g_index]
            normals_b = source_pixels[b_index]
            distance = get_distance(normals_r, normals_g, normals_b)
            if distance < best_distance:
                best_distance = distance
                # Copy pixels for normals image
                result_pixels[r_index] = normals_r
                result_pixels[g_index] = normals_g
                result_pixels[b_index] = normals_b

    result_image.pixels[:] = result_pixels
    result_image.update()

def combine_color_images(source_images, result_image):
    source_pixel_datas = [list(source_image.pixels) for source_image in source_images]  # list() is MUCH faster
    num_pixels = int(len(source_pixel_datas[0]) / 4)

    # Initialize pixel data for new image
    result_pixels = [0, 0, 0, 1] * num_pixels

    # For each pixel, go image by image and use the first pixel
    # that isn't transparent. (Am assuming values from the lower images are better.)
    for r_index in range(0, num_pixels * 4, 4):
        g_index = r_index + 1
        b_index = r_index + 2
        a_index = r_index + 3
        for source_pixels in source_pixel_datas:
            alpha = source_pixels[a_index]
            if alpha == 1:
                result_pixels[r_index] = source_pixels[r_index]
                result_pixels[g_index] = source_pixels[g_index]
                result_pixels[b_index] = source_pixels[b_index]
                break

    result_image.pixels[:] = result_pixels
    result_image.update()

def duplicate_obj(obj, context=bpy.context, name=None):
    hidden = obj.hide_get()
    if hidden:
        obj.hide_set(False)
    select_only([obj], context=context)
    bpy.ops.object.duplicate_move()
    duplicate = context.active_object
    if name is not None:
        duplicate.name = name
    if hidden:
        obj.hide_set(True)
    return duplicate

def get_orig_color_image(target):
    material = target.data.materials[0]
    nodes = material.node_tree.nodes
    bsdf = nodes['Principled BSDF']
    color_links = bsdf.inputs['Base Color'].links
    nodes = [link.from_node for link in color_links if isinstance(link.from_node, bpy.types.ShaderNodeTexImage)]
    return nodes[0].image

def get_orig_combo_image(target):
    material = target.data.materials[0]
    nodes = material.node_tree.nodes
    bsdf = nodes['Principled BSDF']
    metallic_links = bsdf.inputs['Metallic'].links
    separate_color_node = [link.from_node for link in metallic_links if isinstance(link.from_node, bpy.types.ShaderNodeSeparateColor)][0]
    color_links = separate_color_node.inputs['Color'].links
    image_texture_node = [link.from_node for link in color_links if isinstance(link.from_node, bpy.types.ShaderNodeTexImage)][0]
    return image_texture_node.image

def filter_object(self, obj):
    return obj.type == 'MESH'
