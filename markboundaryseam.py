import bpy

class MarkBoundarySeam(bpy.types.Operator):
    'Mark Boundary Seam'
    bl_idname = 'uv.markboundaryseam'
    bl_label = 'WMark Boundary Seam'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.mesh.select_mode(type='EDGE')
        bpy.ops.mesh.region_to_loop()
        bpy.ops.mesh.mark_seam(clear=False)
        bpy.ops.mesh.loop_to_region()
        bpy.ops.mesh.select_mode(type='FACE')

        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        return (
            context.mode == 'EDIT_MESH' and
            context.object is not None
        )
