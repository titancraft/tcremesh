import bpy
from mathutils import Vector

def plane_cut_average(obj, context=bpy.context):
    select_mode = list(bpy.context.tool_settings.mesh_select_mode)
    obj.update_from_editmode()

    selected_indices = []
    for ii, vertex in enumerate(obj.data.vertices):
        if vertex.select:
            selected_indices.append(ii)
    if len(selected_indices) == 0:
        raise RuntimeError('No vertices selected.')

    bpy.ops.mesh.select_mode(type='EDGE')
    bpy.ops.mesh.region_to_loop()
    bpy.ops.mesh.edge_face_add()
    obj.update_from_editmode()

    position = Vector()
    count = 0
    for vertex in obj.data.vertices:
        if vertex.select:
            position += vertex.co
            count += 1
    position /= count

    normal = None
    for face in obj.data.polygons:
        if face.select:
            normal = face.normal.copy()
    if normal is None:
        raise RuntimeError('Failed to auto generate faces.')
    bpy.ops.mesh.delete(type='ONLY_FACE')

    bpy.ops.mesh.select_mode(type='VERT')
    bpy.ops.object.mode_set(mode='OBJECT')
    vertices = obj.data.vertices
    for ii in selected_indices:
        vertices[ii].select = True
    bpy.ops.object.mode_set(mode='EDIT')

    bpy.ops.mesh.select_more()
    bpy.ops.mesh.select_more()
    bpy.ops.mesh.bisect(plane_co=position, plane_no=normal)
    bpy.context.tool_settings.mesh_select_mode = select_mode

class PlaneCutAverage(bpy.types.Operator):
    'PlaneCutAverage'
    bl_idname = 'mesh.plane_cut_average'        
    bl_label = 'Plane Cut Average'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        plane_cut_average(context.active_object, context=context)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        if (context.mode != 'EDIT_MESH' or
            context.active_object is None or
            context.active_object.type != 'MESH'):
            return False

        return True
