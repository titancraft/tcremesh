import bpy
from enum import Enum
from tcremesh.projectbyparts import select_only


def combine_bake_results(source_images, result_image):
    import math
    target_r = 128/255
    target_g = 128/255
    target_b = 1

    def get_distance(rr, gg, bb):
        return math.sqrt((rr - target_r)**2 + (gg - target_g) ** 2 + (bb - target_b) **2)

    image_pixel_datas = [list(source_image.pixels) for source_image in source_images]  # list() is MUCH faster
    num_pixels = int(len(image_pixel_datas[0]) / 4)

    new_pixels = [1] * (num_pixels * 4)
    for r_index in range(0, num_pixels * 4, 4):
        g_index = r_index + 1
        b_index = r_index + 2
        best_distance = 9999
        for pixels in image_pixel_datas:
            rr = pixels[r_index]
            gg = pixels[g_index]
            bb = pixels[b_index]
            distance = get_distance(rr, gg, bb)
            if distance < best_distance:
                best_distance = distance
                new_pixels[r_index] = rr
                new_pixels[g_index] = gg
                new_pixels[b_index] = bb

    result_image.pixels[:] = new_pixels
    result_image.update()


def get_ray_distances(start, end, num_steps):
    steps = []
    current = start
    step_size = (end - start) / (num_steps - 1)
    while start < end and current <= end:
        step = round(current * 100) / 100
        steps.append(step)
        current += step_size

    return steps

def delete_objects(*args, context=bpy.context):
    select_only(args, context=context)
    bpy.ops.object.delete()

def duplicate_obj(obj, context=bpy.context, name=None):
    hidden = obj.hide_get()
    if hidden:
        obj.hide_set(False)
    select_only([obj], context=context)
    bpy.ops.object.duplicate_move()
    duplicate = context.active_object
    if name is not None:
        duplicate.name = name
    if hidden:
        obj.hide_set(True)
    return duplicate

def get_or_create_material(target):
    materials = target.data.materials
    if len(materials) > 0:
        return materials[0]

    # No materials, create a new one
    material = bpy.data.materials.new('Material')
    material.use_nodes = True
    materials.append(material)
    return material

def get_or_create_normal_map_node(target, nodes, links, bsdf_node):
    normals_links = bsdf_node.inputs['Normal'].links
    if len(normals_links) > 0:
        nodes = [link.from_node for link in normals_links if isinstance(link.from_node, bpy.types.ShaderNodeNormalMap)]
        if len(nodes) > 0:
            return nodes[0]

    # No normal map node found, create a new one and link it
    normal_map_node = nodes.new('ShaderNodeNormalMap')
    normal_map_node.uv_map = target.data.uv_layers[0].name
    links.new(bsdf_node.inputs['Normal'], normal_map_node.outputs['Normal'])
    return normal_map_node

def get_or_create_normals_texture_node(target, nodes, links, bsdf_node):
    normal_map_node = get_or_create_normal_map_node(target, nodes, links, bsdf_node)
    color_links = normal_map_node.inputs['Color'].links
    if len(color_links) > 0:
        nodes = [link.from_node for link in color_links if isinstance(link.from_node, bpy.types.ShaderNodeTexImage)]
        if len(nodes) > 0:
            return nodes[0]

    # No image texture found, create a new one and link it
    texture_node = nodes.new('ShaderNodeTexImage')
    links.new(normal_map_node.inputs['Color'], texture_node.outputs['Color'])
    return texture_node

class State(Enum):
    INITIAL_PROMPT = 0
    BAKING = 1
    CLEANUP = 2

def filter_object(self, obj):
    return obj.type == 'MESH'

class NormalBake(bpy.types.Operator):
    'Normal Bake'
    bl_idname = 'object.normalbake'
    bl_label = 'Normal Bake'
    bl_options = {'REGISTER', 'UNDO'}

    _timer = None
    state = None

    texture_dimension: bpy.props.EnumProperty(
        name='Size',
        items=[
            ('256', '256 x 256 px', ''),
            ('512', '512 x 512 px', ''),
            ('1024', '1024 x 1024 px', ''),
            ('2048', '2048 x 2048 px', ''),
            ('4096', '4096 x 4096 px', ''),
        ],
        default='1024',
    )
    margin: bpy.props.IntProperty(
        name='Margin',
        min=0,
        max=32,
        default=2,
        subtype='PIXEL',
    )
    combine_multiple_bakes: bpy.props.BoolProperty(
        name='Combine bake results for multiple distances',
        default=False,
    )
    num_steps: bpy.props.IntProperty(
        name='Steps',
        min=2,
        max=10,
        step=1,
        default=2,
    )
    ray_distance: bpy.props.FloatProperty(
        name='Ray Distance',
        min=0.01,
        step=0.01,
        default=0.15,
    )
    ray_distance_start: bpy.props.FloatProperty(
        name='Start',
        min=0.01,
        step=0.01,
        default=0.05,
    )
    ray_distance_end: bpy.props.FloatProperty(
        name='End',
        min=0.01,
        step=0.01,
        default=0.4,
    )
    max_ray_distance: bpy.props.FloatProperty(
        name='Max Ray Distance',
        min=0,
        step=0.01,
        default=0,
    )

    @classmethod
    def on_register(cls):
        bpy.types.Scene.normal_bake_high_poly = bpy.props.PointerProperty(
            name='High Poly',
            type=bpy.types.Object,
            poll=filter_object,
        )
        bpy.types.Scene.normal_bake_low_poly = bpy.props.PointerProperty(
            name='Low Poly',
            type=bpy.types.Object,
            poll=filter_object,
        )

    @classmethod
    def on_unregister(cls):
        del bpy.types.Scene.normal_bake_high_poly
        del bpy.types.Scene.normal_bake_low_poly

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        layout.separator()

        layout.prop(scene, 'normal_bake_high_poly')
        layout.prop(scene, 'normal_bake_low_poly')
        layout.separator()

        dimensions = layout.row()
        dimensions.prop(self, 'texture_dimension')
        dimensions.prop(self, 'margin')
        layout.separator()

        layout.prop(self, 'combine_multiple_bakes')
        if self.combine_multiple_bakes:
            row = layout.row()
            row.label(text='Ray Distance')
            row.prop(self, 'ray_distance_start')
            row.prop(self, 'ray_distance_end')
            layout.prop(self, 'num_steps')

            steps = get_ray_distances(self.ray_distance_start, self.ray_distance_end, self.num_steps)
            layout.label(text='Distances: ' + ', '.join([str(step) for step in steps]))
        else:
            layout.prop(self, 'ray_distance')

        layout.separator()
        layout.prop(self, 'max_ray_distance')


    def evaluate_state(self, context):
        if self.state == State.INITIAL_PROMPT:
            self.prepare_normal_bake(context)
            self.state = State.BAKING
            self.ray_distance_index = 0
            self.part_index = 0
            return {'PASS_THROUGH'}

        elif self.state == State.BAKING:
            # Bake by parts
            num_matches = len(self.matches)
            if self.part_index >= num_matches:
                # Finished baking parts for one ray distance, move to the next
                if self.ray_distance_index >= len(self.ray_distances) - 1:
                    self.report({'INFO'}, 'Normal baking complete.')
                    self.state = State.CLEANUP
                else:
                    self.part_index = 0
                    self.ray_distance_index += 1
                return {'PASS_THROUGH'}
            elif self.part_index == 0:
                # Do setup for this ray distance
                self.normals_texture_node.image = self.bake_images[self.ray_distance_index]
                context.scene.render.bake.cage_extrusion = self.ray_distances[self.ray_distance_index]

            low_poly_part, high_res_part = self.matches[self.part_index]
            select_only([low_poly_part, high_res_part], context=context)
            context.scene.render.bake.use_clear = self.part_index == 0
            self.report({'INFO'}, f'Baking part {self.part_index+1} of {num_matches}.')
            bpy.ops.object.bake(type='NORMAL')
            self.part_index += 1
            return {'PASS_THROUGH'}

        elif self.state == State.CLEANUP:
            if len(self.ray_distances) > 1:
                # Combine bake results into one image
                combine_bake_results(self.bake_images, self.image)
                self.normals_texture_node.image = self.image

                # Delete extra bake results
                for image in self.bake_images:
                    if image is not self.image:
                        bpy.data.images.remove(image)

            # Remove copy objects
            objects = []
            for low_poly, high_poly in self.matches:
                objects.append(low_poly)
                objects.append(high_poly)
            delete_objects(*objects, context=context)

            # Restore original selection
            select_only(self.orig_selection, context=context)

            # Highlight texture image
            for area in context.screen.areas:
                if area.type == 'IMAGE_EDITOR':
                    area.spaces.active.image = self.image

            return {'FINISHED'}

        return {'PASS_THROUGH'}

    def prepare_normal_bake(self, context):
        from tcremesh.projectbyparts import split_to_parts, match_objects
        from tcremesh.utils import generate_random_string

        select_only([self.low_poly], context=context)
        # bpy.ops.object.shade_smooth()

        # Set up material
        material = get_or_create_material(self.low_poly)
        nodes = material.node_tree.nodes
        links = material.node_tree.links
        bsdf_node = nodes['Principled BSDF']
        normals_texture_node = get_or_create_normals_texture_node(self.low_poly, nodes, links, bsdf_node)
        nodes.active = normals_texture_node

        # Set up image textures to bake to
        dimension = int(self.texture_dimension)
        iid = generate_random_string()
        name = f'{self.low_poly.name} normals {iid}'

        image = normals_texture_node.image
        if image is not None and image.size[0] != dimension:
            # Existing image, wrong size - unset.
            normals_texture_node.image = None
            image = None

        if image is None:
            image = bpy.data.images.new(name, width=dimension, height=dimension, alpha=False)
            normals_texture_node.image = image

        image.pack()
        image.colorspace_settings.name = 'Non-Color'
        self.image = image
        self.nodes = nodes
        self.bsdf_node = bsdf_node
        self.normals_texture_node = normals_texture_node

        if len(self.ray_distances) == 1:
            bake_images = [image]
        else:
            bake_images = []
            for ii, ray_distance in enumerate(self.ray_distances):
                image_name = f'{self.low_poly.name} normals {iid} {ray_distance}'
                image = bpy.data.images.new(image_name, width=dimension, height=dimension, alpha=False)
                bake_images.append(image)
        self.bake_images = bake_images


        # Setup render and image settings
        scene = context.scene
        scene.render.engine = 'CYCLES'
        scene.render.image_settings.file_format='PNG'
        scene.render.bake.use_selected_to_active = True
        scene.render.bake.margin = self.margin
        scene.render.bake.max_ray_distance = self.max_ray_distance
        # scene.render.bake.cage_extrusion = self.ray_distance

        low_poly = duplicate_obj(self.low_poly, context=context, name='_active')
        # bpy.ops.object.mode_set(mode='EDIT')
        # bpy.ops.mesh.select_all(action='SELECT')
        # bpy.ops.mesh.quads_convert_to_tris(quad_method='SHORTEST_DIAGONAL', ngon_method='BEAUTY')
        # bpy.ops.mesh.select_all(action='DESELECT')
        # bpy.ops.object.mode_set(mode='OBJECT')
        high_res = duplicate_obj(self.high_poly, context=context, name='_target')
        # bpy.ops.object.mode_set(mode='EDIT')
        # bpy.ops.mesh.select_all(action='SELECT')
        # bpy.ops.mesh.quads_convert_to_tris(quad_method='SHORTEST_DIAGONAL', ngon_method='BEAUTY')
        # bpy.ops.mesh.select_all(action='DESELECT')
        # bpy.ops.object.mode_set(mode='OBJECT')
        # if hasattr(low_poly.data, 'use_auto_smooth'):
        #     low_poly.data.use_auto_smooth = False
        #     high_res.data.use_auto_smooth = False

        # Split to parts
        # bpy.ops.object.shade_smooth()
        low_poly_parts = split_to_parts(low_poly, context=context)
        # bpy.ops.object.shade_smooth()
        high_poly_parts = split_to_parts(high_res, context=context)

        self.matches = match_objects(low_poly_parts, high_poly_parts)

    def modal(self, context, event):
        if event.type == 'ESC':
            return self.cancel(context)

        if event.type == 'TIMER':
            return self.evaluate_state(context)

        return {'PASS_THROUGH'}

    def execute(self, context):
        scene = context.scene
        self.low_poly = scene.normal_bake_low_poly
        self.high_poly = scene.normal_bake_high_poly
        self.orig_selection = context.selected_objects
        if self.combine_multiple_bakes:
            self.ray_distances = get_ray_distances(self.ray_distance_start, self.ray_distance_end, self.num_steps)
        else:
            self.ray_distances = [round(self.ray_distance * 100) / 100]
        if self.high_poly is None or self.low_poly is None or self.high_poly is self.low_poly:
            self.report({'ERROR_INVALID_CONTEXT'}, f'Must select two meshes.')
            return {'CANCELLED'}

        num_uvs = len(self.low_poly.data.uv_layers)
        if num_uvs == 0:
            self.report({'ERROR'}, f'Low poly object has no UVs!')
            return {'CANCELLED'}
        elif num_uvs > 1:
            self.report({'WARNING'}, f'Multiple UVs found on low poly. Using the first.')

        self.state = State.INITIAL_PROMPT
        self._timer = context.window_manager.event_timer_add(1, window=context.window)
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        context.window_manager.event_timer_remove(self._timer)
        return {'CANCELLED'}

    def invoke(self, context, event):
        # Try to set high and low poly arguments from current selection
        scene = context.scene
        selected = [obj for obj in context.selected_objects if obj.type == 'MESH']

        scene.normal_bake_high_poly = None
        scene.normal_bake_low_poly = None
        if len(selected) == 2:
            # If two objects are selected, look at vertex count to decide high/low poly
            selected.sort(key=lambda obj: len(obj.data.vertices))
            scene.normal_bake_high_poly = selected[1]
            scene.normal_bake_low_poly = selected[0]
        elif len(selected) == 1:
            # If only one objects selected, assume low poly and try to guess high poly
            obj = selected[0]
            scene.normal_bake_low_poly = obj
            if 'lowres' in obj.name:
                name_orig = obj.name.replace('lowres', 'orig')
                name_highres = obj.name.replace('lowres', 'highres')
                if name_orig in scene.objects:
                    scene.normal_bake_high_poly = scene.objects[name_orig]
                elif name_highres in scene.objects:
                    scene.normal_bake_high_poly = scene.objects[name_highres]

        return context.window_manager.invoke_props_dialog(self)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'
