import bpy

def select_only(objects, context=bpy.context):
    bpy.ops.object.select_all(action='DESELECT')
    for obj in objects:
        obj.select_set(True)
    
    context.view_layer.objects.active = objects[0]
'''
Split object to separate shells and return the separated objects
'''
def split_to_parts(obj, context=bpy.context):
    select_only([obj], context=context)
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.separate(type='LOOSE')
    bpy.ops.object.mode_set(mode='OBJECT')
    return context.selected_objects

'''
Given two lists of objects a and b, return a list of pairs of matches between a and b.
Matching is done by matching distance from a bounding box corner.
'''
def match_objects(objects_a, objects_b):
    import math

    objects_b = objects_b[:]  # will be deleting from list, so use a copy

    pairs = []
    for obj_a in objects_a:
        bbox_a = obj_a.bound_box
        corner_a_x = bbox_a[0][0]
        corner_a_y = bbox_a[0][1]
        corner_a_z = bbox_a[0][2]

        def get_distance(obj_b):
            bbox_b = obj_b.bound_box
            corner_b_x = bbox_b[0][0]
            corner_b_y = bbox_b[0][1]
            corner_b_z = bbox_b[0][2]
            distance = math.sqrt(
                (corner_b_x - corner_a_x) ** 2 +
                (corner_b_y - corner_a_y) ** 2 +
                (corner_b_z - corner_a_z) ** 2
            )
            return (obj_b, distance)

        distances = list(map(get_distance, objects_b))
        distances.sort(key=lambda elem: elem[1])
        closest_b = distances[0][0]
        # num = len(pairs) + 1
        # obj_a.name = f'pair {num} a'
        # closest_b.name = f'pair {num} b'
        pairs.append((obj_a, closest_b))
        objects_b.remove(closest_b)

    return pairs

def project_by_parts(active, target, wrap_method='PROJECT', project_limit=0, context=bpy.context):
    hidden = target.hide_get()
    if hidden:
        target.hide_set(False)
    select_only([target], context=context)
    bpy.ops.object.duplicate_move()
    duplicate = context.active_object
    duplicate.name = 'duplicate'
    if hidden:
        target.hide_set(True)

    objects_a = split_to_parts(active, context=context)
    objects_b = split_to_parts(duplicate, context=context)
    for obj_a, obj_b in match_objects(objects_a, objects_b):
        shrinkwrap = obj_a.modifiers.new('_project', 'SHRINKWRAP')
        shrinkwrap.wrap_method = wrap_method
        shrinkwrap.use_negative_direction = True
        shrinkwrap.target = obj_b
        shrinkwrap.project_limit = project_limit
        select_only([obj_a], context=context)
        bpy.ops.object.modifier_apply(modifier=shrinkwrap.name)

    select_only(objects_a, context=context)
    bpy.ops.object.join()
    select_only(objects_b, context=context)
    bpy.ops.object.delete()

    select_only([active], context=context)

class ProjectByParts(bpy.types.Operator):
    'Project by Parts'
    bl_idname = 'object.projectbyparts'        
    bl_label = 'Project by Parts'
    bl_options = {'REGISTER', 'UNDO'}

    wrap_method: bpy.props.EnumProperty(
        name='Wrap Method',
        items=[
            ('PROJECT', 'Project', ''),
            ('NEAREST_SURFACEPOINT', 'Nearest Surface Point', ''),
        ],
        default='PROJECT',
    )

    project_limit: bpy.props.FloatProperty(
        name='Project Limit',
        min=0,
        max=100,
        default=0,
        subtype='DISTANCE',
    )

    def get_target(self, context):
        active = context.active_object

        selected = [obj for obj in context.selected_objects if obj != active]
        if len(selected) == 1:
            return selected[0]

        # Try to auto-select target
        auto_name = active.name.replace('lowres', 'orig').replace('highres', 'orig')
        if auto_name == active.name:
            return None
        try:
            return bpy.data.objects[auto_name]
        except KeyError:
            pass

        auto_name = active.name.replace('lowres', 'highres')
        if auto_name == active.name:
            return None
        try:
            return bpy.data.objects[auto_name]
        except KeyError:
            return None

    def execute(self, context):
        active = context.active_object
        target = self.get_target(context)
        if target is None:
            self.report({'ERROR_INVALID_CONTEXT'}, f'Failed to auto-select target. Select a second mesh for the target.')
            return {'CANCELLED'}

        project_by_parts(
            active,
            target,
            wrap_method=self.wrap_method,
            project_limit=self.project_limit,
            context=context
        )

        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT' and
                context.object is not None and
                context.object.type == 'MESH')
