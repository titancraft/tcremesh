import bpy
from math import radians

FOLLOW_DATA = {
    'pelvis': {
        'position': (0, -0.783, -16.378),
    },
    'shoulder': {
        'position': (-3.54668, -0.834788, -27.2206),
        'rotation': (2.13742, -27.1736, -1.65612),
    },
    'arm': {
        'position': (-2.615, -0.979, -26.147),
    },
    'torso': {
        'position': (0, -0.006, -20.84),
    },
    'leg': {
        'position': (-1.924, -0.277, -17.28),
    },
    'foot': {
        'position': (-2.74, 0.15, -9.42),
    },
    'hand': {
        'position': (-8.76568, 1.83458, -19.9522),
        'rotation': (3.50891, -54.4655, 36.1957),
    },
    'headgear': {
        'position': (0, 1.45675, -30.8785),
        'rotation': (2.30883, 0, 0),
    },
    'hat': {
        'position': (0, 0.197135, -31.2738),
        'rotation': (2.16091, 0, 0),
    },
    'head': {
        'position': (0, -0.336237, -28.8085),
    },
    'shield': {
        'position': (5.06065, -2.718, -6.79569),
        'rotation': (48.0154, 37.5116, -54.303),
    },
    'pack': {
        'position': (0, -2.51898, -25.45)
    },
    'tail': {
        'position': (0, -0.856236, -19.0578),
    },
    'horn': {
        'position': (0.247101, -22.4158, -21.6784),
        'rotation': (-46.1858, -20.9118, -18.396),
    },
    'mustache': {
        'position': (0, 2.46813, -28.9523),
    },
    'belt': {
        'position': (0, 0.051555, -19.3737),
    },
    # temporary category used to move old belts to new position.
    'beltupdate': {
        'position': (0, 0.834555, -2.9957),
    },
    # temporary category used to move heraldry to new position.
    'heraldry': {
        'position': (0, 2.82, 0),
        'rotation': (0, 0, -90),
        'first_transform': 'rotate',
    },
    # legs linker: 0, -0.277 mm, -16.4 mm
    # Insert more types here
}

NAME_FOLLOW_TYPE_MAP = {
    'belt': 'belt',
    'overcoat': 'pelvis',
    'pants': 'pelvis',
    'shirt': 'pelvis',

    'sleeve': 'arm',

    'shoulder': 'shoulder',

    'collar': 'torso',
    'long': 'torso',

    'shoes': 'leg',

    'foot': 'foot',

    'headgear': 'head',
    'hair': 'head',
    'head': 'head',

    'glove': 'hand',

    # Insert more name mappings here
}

'''
Given an object, try to guess from its name which follow type it should use.
Return None if couldn't guess.
'''
def guess_obj_type(obj):
    import re
    tokens = re.split(' |_', obj.name)  # split by space or underscore
    return NAME_FOLLOW_TYPE_MAP.get(tokens[0], 'notset')

def get_autoposition_opts(scene, context):
    types = list(FOLLOW_DATA.keys())

    # Put object type first in list so it is auto-selected
    obj_type = guess_obj_type(context.object)
    try:
        types.remove(obj_type)
    except ValueError:
        pass  # obj_type was "notset"

    types.insert(0, obj_type)

    # Generate options for dropdown menu.
    # Expects 3-tuples like ('<value>', '<Pretty Name>', '<not sure>')
    options = [(
        type_name,
        type_name.capitalize() if type_name != 'notset' else '-----',
        ''
    ) for type_name in types]

    return options

class AutoPosition(bpy.types.Operator):
    'Auto Position'
    bl_idname = 'object.autoposition'
    bl_label = 'Auto Position'
    bl_options = {'REGISTER', 'UNDO'}
    bl_property = 'obj_type'

    obj_type: bpy.props.EnumProperty(
        name='Follow Type',
        items=get_autoposition_opts
    )
    reverse: bpy.props.BoolProperty(
        name='Reverse',
    )

    def execute(self, context):
        import math

        follow_data = FOLLOW_DATA.get(self.obj_type, None)
        if follow_data is None:
            return {'CANCELLED'}

        reverse_modifier = -1 if self.reverse else 1

        position = [reverse_modifier * elem for elem in follow_data.get('position', (0, 0, 0))]
        rotation_euler = [reverse_modifier * math.radians(elem) for elem in follow_data.get('rotation', (0, 0, 0))]
        for obj in context.selected_objects:
            if follow_data.get('first_transform') == 'rotate':
                obj.rotation_euler = rotation_euler
                # bpy.ops.object.transform_apply(location=True)
                obj.location = position
            else:
                obj.location = position
                # bpy.ops.object.transform_apply(location=True)
                obj.rotation_euler = rotation_euler

        return {'FINISHED'}

    def invoke(self, context, event):
        # Apply current position / rotation
        bpy.ops.object.transform_apply(location=True, rotation=True)

        if self.obj_type == 'notset':
            return context.window_manager.invoke_props_dialog(self)
        else:
            return self.execute(context)

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT' and
                context.object is not None and
                context.object.select_get() == True)
