import bpy
from enum import Enum

class RemeshStatus(object):
    def __init__(self):
        self.complete = False

remesh_status = RemeshStatus()

# Update QuadRemesher operator to update remesh_status when finished

from quad_remesher_1_3 import QREMESHER_OT_remesh as RemeshOperator
if not hasattr(RemeshOperator, 'modal_orig'):
    RemeshOperator.modal_orig = RemeshOperator.modal
    def modal_wrap(modal_func):
        def wrap(self, context, event):
            ret, = retset = modal_func(self, context, event)
            if ret in {'FINISHED'}:
                remesh_status.complete = True
            return retset
        return wrap

    RemeshOperator.modal = modal_wrap(RemeshOperator.modal)

def start_quad_remesh(quad_count, adaptive_size=50, adapt_quad_count=True,
                      detect_hard_edges=False, x_symmetry=False, use_vertex_color=False,
                      context=bpy.context):
    remesh_status.complete = False

    qremesher = context.scene.qremesher
    qremesher.target_count = quad_count
    qremesher.adaptive_size = adaptive_size
    qremesher.adapt_quad_count = adapt_quad_count
    qremesher.use_vertex_color = use_vertex_color
    qremesher.use_materials = True
    qremesher.use_normals = False
    qremesher.autodetect_hard_edges = detect_hard_edges
    qremesher.symmetry_x = x_symmetry
    qremesher.symmetry_y = False
    qremesher.symmetry_z = False
    qremesher.hide_input = False

    bpy.ops.qremesher.remesh()

class State(Enum):
    INITIAL_PROMPT = 0
    QUADIFY = 1

class Quadify(bpy.types.Operator):
    'Quadify'
    bl_idname = 'object.quadify'        
    bl_label = 'Quadify'
    bl_options = {'REGISTER', 'UNDO'}
    bl_property = 'quad_count'

    _timer = None
    state = None

    quad_count: bpy.props.IntProperty(
        name='Quad Count',
        description='Approximate number of faces for low res mesh.',
        min=100,
        max=20000,
        default=1500,
    )
    adaptive_size: bpy.props.IntProperty(
        name='Adaptive Size',
        min=0,
        max=100,
        default=50,
        subtype='PERCENTAGE',
    )
    adapt_quad_count: bpy.props.BoolProperty(
        name='Adapt Quad Count',
        default=True,
    )
    use_vertex_color: bpy.props.BoolProperty(
        name='Use Vertex Color',
        default=False,
    )
    detect_hard_edges: bpy.props.BoolProperty(
        name='Detect Hard Edges',
        default=False,
    )
    x_symmetry: bpy.props.BoolProperty(
        name='X Symmetry',
        default=False,
    )
    num_subdivisions: bpy.props.IntProperty(
        name='Num Subdivisions',
        min=0,
        max=3,
        default=2,
    )

    def evaluate_state(self, context):
        from tcremesh.projectbyparts import project_by_parts
        from tcremesh.subdivide import subdivide

        if self.state == State.INITIAL_PROMPT:
            self.orig = context.active_object
            if 'orig' not in self.orig.name:
                self.orig.name = f'{self.orig.name} orig'

            start_quad_remesh(
                self.quad_count,
                adaptive_size=self.adaptive_size,
                adapt_quad_count=self.adapt_quad_count,
                detect_hard_edges=self.detect_hard_edges,
                x_symmetry=self.x_symmetry,
                use_vertex_color=self.use_vertex_color,
                context=context,
            )

            self.state = State.QUADIFY
        elif self.state == State.QUADIFY:
            if remesh_status.complete:
                # Quad remesher finished
                self.orig.hide_set(True)
                remeshed = context.active_object
                for key, value in self.orig.data.items():
                    remeshed.data[key] = value
                remeshed.name = self.orig.name.replace('orig', 'lowres')

                for _ in range(self.num_subdivisions):
                    subdivide(remeshed, num_subdivisions=1, context=context)
                    project_by_parts(remeshed, self.orig, context=context)

                return {'FINISHED'}

        return {'PASS_THROUGH'}

    def modal(self, context, event):
        if event.type == 'ESC':
            return self.cancel(context)

        if event.type == 'TIMER':
            return self.evaluate_state(context)

        return {'PASS_THROUGH'}

    def execute(self, context):
        self.state = State.INITIAL_PROMPT
        self._timer = context.window_manager.event_timer_add(1, window=context.window)
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        context.window_manager.event_timer_remove(self._timer)
        return {'CANCELLED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT' and
                context.object is not None and
                context.object.type == 'MESH')
