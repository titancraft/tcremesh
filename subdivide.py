import bpy

def subdivide(obj, num_subdivisions=1, smooth=False, context=bpy.context):
    from tcremesh.projectbyparts import select_only

    orig_num_vertices = len(obj.data.vertices)

    select_only([obj], context=context)
    # modifier = obj.modifiers.new('_subdivide', 'SUBSURF')
    # modifier.levels = num_subdivisions
    # modifier.subdivision_type = 'CATMULL_CLARK' if smooth else 'SIMPLE'
    # bpy.ops.object.modifier_apply(modifier=modifier.name)
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.subdivide(
        number_cuts=num_subdivisions,
        smoothness=1.0 if smooth else 0,
    )
    bpy.ops.object.mode_set(mode='OBJECT')

    checkpoint_vertex_groups = [group for group in obj.vertex_groups if group.name.startswith('_decimate')]
    group = obj.vertex_groups.new(name=f'_decimate {len(checkpoint_vertex_groups)}')
    vertex_indices = list(range(orig_num_vertices))
    group.add(vertex_indices, 1, 'REPLACE')

    name = obj.name.replace('lowres', 'highres')
    if '.' in name:
        name = name[:name.find('.')]
    obj.name = name

class Subdivide(bpy.types.Operator):
    'Subdivide'
    bl_idname = 'object.subdivide'        
    bl_label = 'Subdivide'
    bl_options = {'REGISTER', 'UNDO'}

    smooth: bpy.props.BoolProperty(
        name='Smooth',
        default=True,
    )

    def execute(self, context):
        for obj in context.selected_objects:
            subdivide(obj, num_subdivisions=1, smooth=self.smooth, context=context)

        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT' and
                context.object is not None and
                context.object.type == 'MESH')
