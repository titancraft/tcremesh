bl_info = {
    'name': 'TitanCraft Remesh Tools',
    'blender': (4, 0, 0),
    'category': 'Object',
    'author': 'Andrew Stockton',
    'version': '1.0.0',
}
import bpy
import importlib


if 'stats' in locals():
    importlib.reload(stats)
else:
    from titancraftutils import stats

# Remesh Menu

support_quadify = False
if 'quadify' in locals():
    importlib.reload(quadify)
    support_quadify = True
else:
    try:
        from . import quadify
        support_quadify = True
    except Exception as err:
        pass

if 'unsubdivide' in locals():
    importlib.reload(unsubdivide)
else:
    from . import unsubdivide

if 'subdivide' in locals():
    importlib.reload(subdivide)
else:
    from . import subdivide

if 'projectbyparts' in locals():
    importlib.reload(projectbyparts)
else:
    from . import projectbyparts

if 'autoposition' in locals():
    importlib.reload(autoposition)
else:
    from . import autoposition

if 'normalbake' in locals():
    importlib.reload(normalbake)
else:
    from . import normalbake

if 'bakehightolow' in locals():
    importlib.reload(bakehightolow)
else:
    from . import bakehightolow

if 'applyuvmaterial' in locals():
    importlib.reload(applyuvmaterial)
else:
    from . import applyuvmaterial

if 'compresssleeves' in locals():
    importlib.reload(compresssleeves)
else:
    from . import compresssleeves

remesh_classes = [
    unsubdivide.Unsubdivide,
    subdivide.Subdivide,
    projectbyparts.ProjectByParts,
    autoposition.AutoPosition,
    normalbake.NormalBake,
    bakehightolow.BakeHighToLow,
    applyuvmaterial.ApplyUvMaterial,
    compresssleeves.CompressSleeves,
]
if support_quadify:
    remesh_classes.insert(0, quadify.Quadify)

class OBJECT_MT_remesh(bpy.types.Menu):
    bl_idname = 'OBJECT_MT_remesh'
    bl_label = 'Remesh'

    def draw(self, context):
        layout = self.layout
        for class_ in remesh_classes:
            layout.operator(class_.bl_idname)

        layout.menu(OBJECT_MT_morph.bl_idname)  # Morph Submenu

def remesh_menu_func(self, context):
    layout = self.layout
    layout.menu(OBJECT_MT_remesh.bl_idname)

def register_remesh_menu():
    bpy.utils.register_class(OBJECT_MT_remesh)
    register_fn = getattr(OBJECT_MT_remesh, 'on_register', None)
    if register_fn is not None:
        register_fn()
    for class_ in remesh_classes:
        bpy.utils.register_class(class_)
        register_fn = getattr(class_, 'on_register', None)
        if register_fn is not None:
            register_fn()
    bpy.types.VIEW3D_MT_object.append(remesh_menu_func)

def unregister_remesh_menu():
    bpy.utils.unregister_class(OBJECT_MT_remesh)
    unregister_fn = getattr(OBJECT_MT_remesh, 'on_unregister', None)
    if unregister_fn is not None:
        unregister_fn()
    bpy.types.VIEW3D_MT_object.remove(remesh_menu_func)
    for class_ in remesh_classes:
        bpy.utils.unregister_class(class_)
        unregister_fn = getattr(class_, 'on_unregister', None)
        if unregister_fn is not None:
            unregister_fn()







# Remesh -> Morphs Submenu

if 'splitmorphs' in locals():
    importlib.reload(splitmorphs)
else:
    from .morphs import splitmorphs

if 'createhighresmorphs' in locals():
    importlib.reload(createhighresmorphs)
else:
    from .morphs import createhighresmorphs

# if 'loadzmorphs' in locals():
#     importlib.reload(loadzmorphs)
# else:
#     from .morphs import loadzmorphs

morph_classes = [
    splitmorphs.SplitMorphs,
    createhighresmorphs.CreateHighResMorphs,
    # loadzmorphs.LoadZMorphs,
]

class OBJECT_MT_morph(bpy.types.Menu):
    bl_idname = 'OBJECT_MT_morph'
    bl_label = 'Morph'

    def draw(self, context):
        layout = self.layout
        for class_ in morph_classes:
            layout.operator(class_.bl_idname)

def register_morph_menu():
    bpy.utils.register_class(OBJECT_MT_morph)
    register_fn = getattr(OBJECT_MT_morph, 'on_register', None)
    if register_fn is not None:
        register_fn()
    for class_ in morph_classes:
        bpy.utils.register_class(class_)
        register_fn = getattr(class_, 'on_register', None)
        if register_fn is not None:
            register_fn()

def unregister_morph_menu():
    bpy.utils.unregister_class(OBJECT_MT_morph)
    unregister_fn = getattr(OBJECT_MT_morph, 'on_unregister', None)
    if unregister_fn is not None:
        unregister_fn()
    for class_ in morph_classes:
        bpy.utils.unregister_class(class_)
        unregister_fn = getattr(class_, 'on_unregister', None)
        if unregister_fn is not None:
            unregister_fn()







# Additional UV Operators

if 'markboundaryseam' in locals():
    importlib.reload(markboundaryseam)
else:
    from . import markboundaryseam

uv_classes = [
    markboundaryseam.MarkBoundarySeam,
]

def uv_menu_func(self, context):
    for class_ in uv_classes:
        self.layout.operator(class_.bl_idname)

def register_uv_operators():
    for class_ in uv_classes:
        bpy.utils.register_class(class_)
        register_fn = getattr(class_, 'on_register', None)
        if register_fn is not None:
            register_fn()
    bpy.types.VIEW3D_MT_uv_map.append(uv_menu_func)

def unregister_uv_operators():
    for class_ in uv_classes:
        bpy.utils.unregister_class(class_)
        unregister_fn = getattr(class_, 'on_unregister', None)
        if unregister_fn is not None:
            unregister_fn()
    bpy.types.VIEW3D_MT_uv_map.remove(uv_menu_func)






# Edut ioeratirs

if 'planecutaverage' in locals():
    importlib.reload(planecutaverage)
else:
    from . import planecutaverage

if 'markremesherguide' in locals():
    importlib.reload(markremesherguide)


else:
    from . import markremesherguide

edit_classes = [
    planecutaverage.PlaneCutAverage,
    markremesherguide.MarkRemesherGuide,
]

def edit_menu_func(self, context):
    for class_ in edit_classes:
        self.layout.operator(class_.bl_idname)

def register_edit_operators():
    for class_ in edit_classes:
        bpy.utils.register_class(class_)
        register_fn = getattr(class_, 'on_register', None)
        if register_fn is not None:
            register_fn()
    bpy.types.VIEW3D_MT_edit_mesh.append(edit_menu_func)

def unregister_edit_operators():
    for class_ in edit_classes:
        bpy.utils.unregister_class(class_)
        unregister_fn = getattr(class_, 'on_unregister', None)
        if unregister_fn is not None:
            unregister_fn()
    bpy.types.VIEW3D_MT_edit_mesh.remove(edit_menu_func)





# All

def register():
    register_remesh_menu()
    register_morph_menu()
    register_uv_operators()
    register_edit_operators()


def unregister():
    unregister_remesh_menu()
    unregister_morph_menu()
    unregister_uv_operators()
    unregister_edit_operators()

if __name__ == '__main__':
    register()
