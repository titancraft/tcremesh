import bpy
import random
from mathutils import Vector

def mark_remesher_guide(obj, context=bpy.context):
    if len(obj.data.materials) == 0:
        default = bpy.data.materials.new('Default')
        default.use_nodes = True
        default_bsdf = default.node_tree.nodes.get('Principled BSDF')
        default_bsdf.inputs['Base Color'].default_value = (0.7, 0.7, 0.7, 1)
        default.diffuse_color = (0.7, 0.7, 0.7, 1)
        obj.data.materials.append(default)

    select_mode = list(bpy.context.tool_settings.mesh_select_mode)
    bpy.ops.mesh.select_mode(type='EDGE')
    bpy.ops.mesh.plane_cut_average()
    obj.update_from_editmode()
    cut_edges = [ii for ii, edge in enumerate(obj.data.edges) if edge.select]

    bpy.ops.mesh.select_more()
    bpy.ops.object.mode_set(mode='OBJECT')
    material_indices = set()
    for face in obj.data.polygons:
        if face.select:
            material_indices.add(face.material_index)
    bpy.ops.object.mode_set(mode='EDIT')

    if len(material_indices) > 1:
        raise RuntimeError('Cut cannot pass through multiple materials.')

    # Reset selection to cut region
    bpy.ops.object.mode_set(mode='OBJECT')
    for face in obj.data.polygons:
        face.select = False
    for ii, edge in enumerate(obj.data.edges):
        edge.select = ii in cut_edges
    bpy.ops.object.mode_set(mode='EDIT')

    # Select inner area
    bpy.ops.mesh.loop_to_region()

    bpy.ops.mesh.select_mode(type='FACE')
    index = list(material_indices)[0]
    for ii, material in enumerate(obj.data.materials):
        if ii == index:
            continue
        obj.active_material_index = ii
        bpy.ops.object.material_slot_deselect()

    # Check what materials are bordering this selection
    bpy.ops.mesh.select_more()
    bpy.ops.object.mode_set(mode='OBJECT')
    bordering_material_indices = set()
    for face in obj.data.polygons:
        if face.select:
            bordering_material_indices.add(face.material_index)
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_less()

    # Find or make a new material
    material_index = -1
    for ii in range(len(obj.data.materials)):
        if ii in bordering_material_indices:
            continue
        material_index = ii
        break

    if material_index == -1:
        material_index = len(obj.data.materials)

        new_material = bpy.data.materials.new('Auto Material')
        new_material.use_nodes = True
        rr = random.random()
        gg = random.random()
        bb = 1 - ((rr + gg) / 2)
        # bb = random.random()

        bsdf = new_material.node_tree.nodes.get('Principled BSDF')
        bsdf.inputs['Base Color'].default_value = (rr, gg, bb, 1)
        new_material.diffuse_color = (rr, gg, bb, 1)

        obj.data.materials.append(new_material)
    obj.active_material_index = material_index
    bpy.ops.object.material_slot_assign()

    bpy.context.tool_settings.mesh_select_mode = select_mode

class MarkRemesherGuide(bpy.types.Operator):
    'MarkRemesherGuide'
    bl_idname = 'mesh.mark_remesher_guide'        
    bl_label = 'Mark Remesher Guide'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        mark_remesher_guide(context.active_object, context=context)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        if (context.mode != 'EDIT_MESH' or
            context.active_object is None or
            context.active_object.type != 'MESH'):
            return False

        return True
