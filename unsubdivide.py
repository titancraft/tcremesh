import bpy

def unsubdivide(obj, context=bpy.context):
    checkpoint_vertex_groups = [group for group in obj.vertex_groups if group.name.startswith('_decimate')]
    checkpoint_vertex_groups.sort(key=lambda xx: xx.name)
    checkpoint_group = checkpoint_vertex_groups[-1]

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_mode(type='VERT')
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    vertices = obj.data.vertices
    checkpoint_group_idx = checkpoint_group.index
    for vertex in obj.data.vertices:
        for vertex_group in vertex.groups:
            if vertex_group.group == checkpoint_group_idx:
                vertex.select = True

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.mesh.dissolve_edges()
    bpy.ops.object.mode_set(mode='OBJECT')

    name = obj.name.replace('highres', 'lowres')
    if '.' in name:
        name = name[:name.find('.')]
    obj.name = name

    obj.vertex_groups.remove(checkpoint_group)

class Unsubdivide(bpy.types.Operator):
    'Unsubdivide'
    bl_idname = 'object.unsubdivide'        
    bl_label = 'Unsubdivide'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        unsubdivide(context.active_object, context=context)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        if (context.mode != 'OBJECT' or
            context.active_object is None or
            context.active_object.type != 'MESH'):
            return False

        checkpoint_vertex_groups = [group for group in context.active_object.vertex_groups if group.name.startswith('_decimate')]
        return len(checkpoint_vertex_groups) > 0
