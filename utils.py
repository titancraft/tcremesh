import bpy

def find_highres_lowres(mesh_builder_id):
    highres = None
    lowres = None
    for obj in bpy.data.objects:
        print(obj)
        if obj.data.get('titancraft_id') != mesh_builder_id:
            continue
        if not obj.name.endswith(('_highres', ' highres')):
            continue
        lowres_name = obj.name.replace('highres', 'lowres')
        try:
            lowres = bpy.data.objects[lowres_name]
        except KeyError:
            continue
        highres = obj
        break

    return highres, lowres

def import_gltf(context, path):
    bpy.ops.import_scene.gltf(filepath=path, merge_vertices=True, bone_heuristic='TEMPERANCE')

    armature = context.view_layer.objects.active
    mesh = armature.children[0]

    # Scale down to normal size
    armature.scale *= 0.001
    select_only([armature, mesh], context=context)
    bpy.ops.object.transform_apply(scale=True)

    # Merge vertices
    select_only(mesh, context=context)
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.remove_doubles(threshold=0.0001)
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')

    # Turn off auto smooth, it looks super bad
    if hasattr(mesh.data, 'use_auto_smooth'):
        mesh.data.use_auto_smooth = False

    # Remove existing materials
    while len(mesh.data.materials) > 0:
        bpy.ops.object.material_slot_remove()

    return armature, mesh

def import_gltf_from_url(context, url):
    import requests
    import tempfile
    response = requests.get(url)
    with tempfile.NamedTemporaryFile(mode='wb', delete=False) as fh:
        fh.write(response.content)
        return import_gltf(context, fh.name)

def select_only(objects, context=bpy.context):
    bpy.ops.object.select_all(action='DESELECT')
    if not (isinstance(objects, list) or isinstance(objects, set)):
        objects = [objects]

    for obj in objects:
        obj.select_set(True)
    
    context.view_layer.objects.active = objects[0]

def mark_seams_from_islands(target, context):
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    try:
        bpy.ops.uv.seams_from_islands()
    except:
        pass
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')

'''
Split object to separate shells and return the separated objects
'''
def split_to_parts(obj, context=bpy.context):
    mode_save = context.active_object.mode
    select_only(obj, context=context)
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.separate(type='LOOSE')
    bpy.ops.object.mode_set(mode=mode_save)
    return context.selected_objects

def import_pillow():
    if 'PIL' in locals():
        return

    import site
    import sys
    sys.path.append(site.getusersitepackages())
    import PIL

def open_image(filepath):
    import os
    filename = os.path.basename(filepath)
    try:
        return bpy.data.images[filename]
    except KeyError:
        pass

    filenames_before = {*[image.name for image in bpy.data.images]}
    bpy.ops.image.open(filepath=filepath)
    filenames_after = {*[image.name for image in bpy.data.images]}
    try:
        new_filename = filenames_after.difference(filenames_before).pop()
        return bpy.data.images[new_filename]
    except KeyError:
        try:
            return bpy.data.images[filename]
        except:
            return bpy.data.images[0]

def open_image_from_url(url):
    import requests
    import tempfile
    response = requests.get(url)
    with tempfile.NamedTemporaryFile(mode='wb', delete=False) as fh:
        fh.write(response.content)
        fh.close()
        return open_image(fh.name)

# See https://stackoverflow.com/a/43690506
def pretty_print_file_size(size, decimal_places=2):
    for unit in ['B', 'KB', 'MB', 'GB', 'TB', 'PB']:
        if size < 1024.0 or unit == 'PB':
            break
        size /= 1024.0
    return f'{size:.{decimal_places}f} {unit}'

def generate_random_string(length=5):
    import random
    import string
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=length))
