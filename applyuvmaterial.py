import bpy
import secrets

class ApplyUvMaterial(bpy.types.Operator):
    'Apply UV Material'
    bl_idname = 'object.apply_uv_material'
    bl_label = 'Apply UV Material'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        target = context.object
        name = 'UV Preview'

        uv_material = bpy.data.materials.get(name)
        if uv_material is None:
            uv_material = bpy.data.materials.new(name)
            uv_material.use_nodes = True
            nodes = uv_material.node_tree.nodes
            links = uv_material.node_tree.links
            bsdf = nodes.get('Principled BSDF')
            image_name = secrets.token_hex(5)
            bpy.ops.image.new(
                name=image_name,
                width=1024,
                height=1024,
                alpha=False,
                generated_type='UV_GRID',
            )
            image = bpy.data.images[image_name]
            image_node = nodes.new('ShaderNodeTexImage')
            image_node.image = image
            links.new(bsdf.inputs['Base Color'], image_node.outputs['Color'])

        if name in target.data.materials:
            index = target.data.materials.find(name)
        else:
            index = len(target.data.materials)
            target.data.materials.append(uv_material)

        target.active_material_index = index
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.object.material_slot_assign()
        bpy.ops.object.mode_set(mode='OBJECT')

        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT' and
                context.object is not None and
                context.object.select_get() == True)
