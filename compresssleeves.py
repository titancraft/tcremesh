import bpy
from mathutils import Vector

POS_END_OF_ARM = Vector((6.3, -3.2, -6.6))
POS_TARGET = Vector((0, 4.5, 0))

__all__ = ['CompressSleeves']

def compress_sleeve(target, arm_obj=None, compress_length=3, transition_length=2, shrinkwrap_offset=0.2):
    transition_length = max(transition_length, 0.001)

    # Assign weight for each vertex based on distance along line - scalar projection
    # to vector between end of arm and target
    group = next((gg for gg in target.vertex_groups if gg.name == 'compress_sleeve'), None)
    if group is None:
        group = target.vertex_groups.new(name='compress_sleeve')

    direction = POS_TARGET - POS_END_OF_ARM
    direction.normalize()
    group.remove(list(range(len(target.data.vertices))))  # remove existing weights
    for ii, vertex in enumerate(target.data.vertices):
        world_pos = target.matrix_world @ vertex.co
        magnitude_along_direction = (world_pos - POS_END_OF_ARM).dot(direction)
        # Translate magnitude along direction into % through transition length
        xx = (magnitude_along_direction - compress_length) / transition_length
        # Invert so 100% shrinkwrap is at end of sleep, 0% shrinkwrap at
        # origin of sleeve. Clamp between 0 and 1.
        weight = min(1, max(1 - xx, 0))
        if weight > 0:
            group.add([ii], weight, 'REPLACE')

    # Add shrinkwrap modifier
    shrinkwrap = target.modifiers.get('compress_sleeve')
    if shrinkwrap is None:
        shrinkwrap = target.modifiers.new('compress_sleeve', 'SHRINKWRAP')

    if arm_obj is not None:
        shrinkwrap.target = arm_obj
    shrinkwrap.vertex_group = group.name
    shrinkwrap.offset = shrinkwrap_offset
    # bpy.ops.object.modifier_apply(modifier=shrinkwrap.name)

class CompressSleeves(bpy.types.Operator):
    'Compress Sleeves'
    bl_idname = 'object.compress_sleeves'
    bl_label = 'Compress Sleeves'
    bl_options = {'REGISTER', 'UNDO'}

    compress_length: bpy.props.FloatProperty(
        name='Fully Compressed Length',
        min=0,
        max=10,
        step=0.1,
        default=3,
    )
    transition_length: bpy.props.FloatProperty(
        name='Transition Length',
        min=0,
        max=10,
        step=0.1,
        default=2,
    )
    shrinkwrap_offset: bpy.props.FloatProperty(
        name='Shrinkwrap Offset',
        min=0,
        max=0.5,
        step=0.01,
        default=0.05,
    )

    def execute(self, context):
        for obj in context.selected_objects:
            compress_sleeve(
                obj,
                arm_obj=context.scene.compresssleeves_arm_obj,
                compress_length=self.compress_length,
                transition_length=self.transition_length,
                shrinkwrap_offset=self.shrinkwrap_offset,
            )

        return {'FINISHED'}

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        layout.prop(scene, 'compresssleeves_arm_obj')
        layout.separator()

        layout.prop(self, 'compress_length')
        layout.prop(self, 'transition_length')
        layout.prop(self, 'shrinkwrap_offset')

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT' and
                context.object is not None and
                context.object.select_get() == True)

    @classmethod
    def on_register(cls):
        bpy.types.Scene.compresssleeves_arm_obj = bpy.props.PointerProperty(
            name='Arm Mesh',
            type=bpy.types.Object,
            poll=filter_object,
        )

    @classmethod
    def on_unregister(cls):
        try:
            del bpy.types.Scene.compresssleeves_arm_obj
        except:
            pass

    def invoke(self, context, event):
        # Try to guess arm object
        scene = context.scene
        if scene.compresssleeves_arm_obj is None:
            scene.compresssleeves_arm_obj = None
            arm_objs = [obj for obj in scene.objects if 'arm' in obj.name and obj.type == 'MESH']
            arm_objs.sort(key=lambda obj: len(obj.data.vertices))
            if len(arm_objs) > 0:
                scene.compresssleeves_arm_obj = arm_objs[-1]

        return context.window_manager.invoke_props_dialog(self)

def filter_object(self, obj):
    return obj.type == 'MESH'
