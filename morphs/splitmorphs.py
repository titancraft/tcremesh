import bpy

def clear_shape_key_values(shape_keys):
    for shape_key in shape_keys:
        shape_key.value = 0

def select_shape_key(obj, name):
    for ii, shape_key in enumerate(obj.data.shape_keys.key_blocks):
        if shape_key.name == name:
            obj.active_shape_key_index = ii
            return

def approx_equals(aa, bb, epsilon=1e-2):
    return abs(aa - bb) <= epsilon

def isolate_side(shape_key, basis, side):
    center_multiplier = 0.5
    left_multiplier = 1 if side == 'left' else 0
    right_multiplier = 1 if side == 'right' else 0

    for ii, basis_vertex in enumerate(basis.data):
        basis_co = basis_vertex.co
        shape_key_vertex_co = shape_key.data[ii].co
        xx = basis_co[0]
        multiplier = center_multiplier if approx_equals(xx, 0) else left_multiplier if xx < 0 else right_multiplier

        xx_delta = (shape_key_vertex_co[0] - basis_co[0]) * multiplier
        yy_delta = (shape_key_vertex_co[1] - basis_co[1]) * multiplier
        zz_delta = (shape_key_vertex_co[2] - basis_co[2]) * multiplier

        shape_key_vertex_co[0] = basis_co[0] + xx_delta
        shape_key_vertex_co[1] = basis_co[1] + yy_delta
        shape_key_vertex_co[2] = basis_co[2] + zz_delta

def create_vertex_groups(obj):
    left_group = obj.vertex_groups.new(name='shape_keys_left')
    right_group = obj.vertex_groups.new(name='shape_keys_right')
    left_indices = []
    middle_indices = []
    right_indices = []

    for vertex in obj.data.vertices:
        xx = vertex.co[0]
        if approx_equals(xx, 0):
            middle_indices.append(vertex.index)
        elif xx < 0:
            left_indices.append(vertex.index)
        else:
            right_indices.append(vertex.index)

    left_group.add(left_indices, 1, 'REPLACE')
    left_group.add(middle_indices, 0.5, 'REPLACE')
    right_group.add(right_indices, 1, 'REPLACE')
    right_group.add(middle_indices, 0.5, 'REPLACE')

    return left_group, right_group

class SplitMorphs(bpy.types.Operator):
    'Split Morphs'
    bl_idname = 'object.splitmorphs'        
    bl_label = 'Split Morphs'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        starting_mode = context.mode
        starting_active_object = context.view_layer.objects.active

        if starting_mode != 'OBJECT':
            self.report({'ERROR_INVALID_CONTEXT'}, 'Must be in OBJECT mode.')
            return {'CANCELLED'}

        orig = context.view_layer.objects.active
        if orig.type != 'MESH':
            self.report({'ERROR_INVALID_CONTEXT'}, 'Must select a mesh.')
            return {'CANCELLED'}

        # Duplicate object and hide original
        bpy.ops.object.duplicate_move()
        obj = context.view_layer.objects.active
        obj.name = f'{orig.name} (split morphs)'
        orig.hide_set(True)

        # Create left and right vertex groups
        left_vertex_group, right_vertex_group = create_vertex_groups(obj)

        # Set all shape key values to 0
        shape_keys = obj.data.shape_keys.key_blocks
        clear_shape_key_values(shape_keys)
        to_split = [shape_key for shape_key in shape_keys if shape_key.name.endswith('lr')]

        basis_shape_key = orig.data.shape_keys.key_blocks[0]
        for left_shape_key in to_split:
            # Duplicate shape key
            left_shape_key.value = 1
            select_shape_key(obj, left_shape_key.name)
            bpy.ops.object.shape_key_move(type='BOTTOM')
            bpy.ops.object.shape_key_add(from_mix=True)
            right_shape_key = shape_keys[obj.active_shape_key_index]
            left_shape_key.value = 0

            # Rename to shape keys to left and right
            right_shape_key.name = left_shape_key.name.replace('lr', 'right')
            left_shape_key.name = left_shape_key.name.replace('lr', 'left')

            # # Set vertex groups
            # left_shape_key.vertex_group = left_vertex_group.name
            # right_shape_key.vertex_group = right_vertex_group.name

            isolate_side(left_shape_key, basis_shape_key, side='left')
            isolate_side(right_shape_key, basis_shape_key, side='right')

        return {'FINISHED'}
