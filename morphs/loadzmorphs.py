import bpy

class LoadZMorphs(bpy.types.Operator):
    'Load ZMorphs'
    bl_idname = 'object.loadzmorphs'        
    bl_label = 'Load ZMorphs'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        starting_mode = context.mode
        starting_active_object = context.view_layer.objects.active

        if starting_mode != 'OBJECT':
            self.report({'ERROR_INVALID_CONTEXT'}, 'Must be in OBJECT mode.')
            return {'CANCELLED'}

        target = context.view_layer.objects.active
        if target.type != 'MESH':
            self.report({'ERROR_INVALID_CONTEXT'}, 'No active mesh.')
            return {'CANCELLED'}

        z_morph_objects = [obj for obj in context.view_layer.objects.selected if obj.name.startswith('zhighres_') or obj.name.startswith('zlowres_')]

        if len(z_morph_objects) == 0:
            self.report({'ERROR_INVALID_CONTEXT'}, 'No valid source objects selected.')
            return {'CANCELLED'}

        # Add shape keys
        if target.data.shape_keys is not None and len(target.data.shape_keys.key_blocks) > 0:
            bpy.ops.object.shape_key_remove(all=True)

        bpy.ops.object.shape_key_add(from_mix=False)
        bpy.ops.object.join_shapes()

        # Rename shape keys
        for ii, shape_key in enumerate(target.data.shape_keys.key_blocks):
            if ii == 0:
                continue
            shape_key.name = shape_key.name[shape_key.name.index('_'):].replace('_', ' ')

        # Delete sources
        context.view_layer.objects.active = z_morph_objects[0]
        target.select_set(False)
        bpy.ops.object.delete(use_global=False)

        # Reselect original mesh
        target.select_set(True)
        context.view_layer.objects.active = target

        return {'FINISHED'}
