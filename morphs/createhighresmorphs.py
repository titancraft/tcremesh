'''

High res mesh
 - Duplicate orig low res to target high res
 - Delete shape keys
 - Subdivide
 - Do shrink wrap to orig high res and apply
 - For each shape key on target low res,
   - Duplicate target low res
   - Delete all shape keys except the one we're on
   - Subdivide
   - Join as shapes on high res
'''

import bpy

SUBDIVIDE_NUM_CUTS = 2
SUBDIVIDE_SMOOTH_FACTOR = 0.5

def clear_shape_key_values(shape_keys):
    for shape_key in shape_keys:
        shape_key.value = 0

def select_shape_key(obj, name):
    for ii, shape_key in enumerate(obj.data.shape_keys.key_blocks):
        if shape_key.name == name:
            obj.active_shape_key_index = ii
            return

def do_subdivide():
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.subdivide(
        number_cuts=SUBDIVIDE_NUM_CUTS,
        smoothness=SUBDIVIDE_SMOOTH_FACTOR,
    )
    bpy.ops.object.mode_set(mode='OBJECT')

def do_decimate(ratio=0.1):
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.decimate(
        ratio=ratio,
        use_symmetry=True,
        symmetry_axis='X',
    )
    bpy.ops.object.mode_set(mode='OBJECT')

class CreateHighResMorphs(bpy.types.Operator):
    'Create High Res Morphs'
    bl_idname = 'object.createhighresmorphs'        
    bl_label = 'Create High Res Morphs'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        starting_mode = context.mode
        starting_active_object = context.view_layer.objects.active

        if not starting_mode == 'OBJECT':
            self.report({'ERROR_INVALID_CONTEXT'}, 'Must be in OBJECT mode.')
            return {'CANCELLED'}

        orig = context.view_layer.objects.active
        # other_selected = [obj for obj in bpy.context.selected_objects if obj != orig]
        # if orig.type != 'MESH' or len(other_selected) != 1:
        #     self.report({'ERROR_INVALID_CONTEXT'}, 'Select low-res and high-res meshes (with low-res active).')
        #     return {'CANCELLED'}

        # orig_highres = other_selected[0]

        # Duplicate object
        # orig_highres.select_set(False)
        bpy.ops.object.duplicate_move()
        target = context.view_layer.objects.active
        target.name = f'{orig.name} (highres morphs)'
        orig.select_set(False)
        target.select_set(True)

        # Delete existing shape keys
        bpy.ops.object.shape_key_remove(all=True)

        # Subdivide
        do_subdivide()
        # do_decimate(ratio=0.1)

        # Project to high res mesh
        # shrinkwrap = target.modifiers.new('create_high_res_shrink', type='SHRINKWRAP')
        # shrinkwrap.target = orig_highres
        # bpy.ops.object.modifier_apply(modifier=shrinkwrap.name)

        # Add Basis shape key
        bpy.ops.object.shape_key_add()

        # Add shape keys
        for orig_shape_key in orig.data.shape_keys.key_blocks:
            shape_key_name = orig_shape_key.name
            if shape_key_name == 'Basis':
                continue

            print(f'Processing {shape_key_name}...')

            # Duplicate orig
            bpy.ops.object.select_all(action='DESELECT')
            orig.select_set(True)
            context.view_layer.objects.active = orig
            bpy.ops.object.duplicate_move()
            copy = context.view_layer.objects.active
            orig.select_set(False)

            # Delete all shape keys except the one we are on
            copy_shape_keys = copy.data.shape_keys.key_blocks
            select_shape_key(copy, shape_key_name)
            bpy.ops.object.shape_key_move(type='TOP')
            bpy.ops.object.shape_key_move(type='TOP')  # first time gets it below Basis, second time moves it to the actual top
            for ii in range(len(copy_shape_keys) - 1):
                copy.active_shape_key_index = 1
                bpy.ops.object.shape_key_remove()

            # Subdivide
            do_subdivide()
            # do_decimate(ratio=0.1)


            # Create new shape key on target
            target.select_set(True)
            context.view_layer.objects.active = target
            bpy.ops.object.join_shapes()
            shape_key = target.data.shape_keys.key_blocks[-1]
            shape_key.name = shape_key_name
            shape_key.vertex_group = copy_shape_keys[0].vertex_group

            # Delete copy
            context.view_layer.objects.active = copy
            target.select_set(False)
            bpy.ops.object.delete()

        # Hide orig, select target
        target.select_set(True)
        context.view_layer.objects.active = target
        orig.hide_set(True)
        # orig_highres.hide_set(True)

        return {'FINISHED'}
